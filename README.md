# QuasisPycies

Python 3 module to calculate diversity indices from NGS data. It consists of two main classes (Clusters and Pileup) and extra functions for convertion and parsing different file formats.

### class Clusters 
	
	Functions:  shannon_entropy
				simpsons_index
				simpsons_index_of_diversity
				simpsons_Reciprocal_Index
				clusters_object_to_fasta
				filtering_clusters_by_percentage
				number_haplotypes
				

### class Pileup
			
	Functions:  nucleotide_diversity
				polymorphic_sites


	Extra functions:
				filtering_clusters_by_percentage
				sam_to_fasta
				fastq_to_fasta
				multiline_fasta_to_singleline
				clusters_to_single_reads
                create_input_for_pileup_class
                create_input_for_clusters_class


## Input file for Clusters Class

For the Clusters class, you need to provide a fasta file with sequences representing the clusters. The fasta file must have number of reads	("size=") on each sequence name.

eg.

	>ReadA;size=180
	AAATTTCCCGGG
	>ReadB;size=20
	TTTAAATTTGGG 

The easieast way to do it is using Usearch (http://www.drive5.com/usearch/download.html) and the command cluster_fast:
```
$ usearch -cluster_fast [fasta input] -id 1.0 -centroids [fasta output] -sizeout
```
The id option specifies the percentage of identity needed for clustering the sequences. I use 1.0 which means that clusters are formed only by identical sequences.

The sizeout option prints the number of reads for each cluster on the end of the sequence name (;size=).

Next step is to use the function multiline_fasta_to_singleline from the quasispycies module. The reason for it is because the output fasta from usearch cluster_fast is a fasta file with the nucleotide sequence information in multiple lines instead in just one line. Most of the scripts on the quasispycies were designed to work with single line fasta, otherwise it would just use the informaton from the first line of each nucleotide sequence.

Finally I recommend filtering the clusters by percentage based on the error rate of your sequencing and PCR. You can do that by sequencing a viral stock or plasmid and checking the percentage of variants. In my experience, using clusters above 2% is a conservative value when you are dealing with Illumina MiSeq amplicon sequencing. To do it you can use the function filtering_clusters_by_percentage. To use it you have to provide the name of the fasta file (single line fasta), the percentage to filter (if you want to filter only clusters above 2% you should write 0.02) and the fasta output file name.

You can use the function 'create_input_for_clusters_class' to filter and parse your fastq to use as input on the Clusters Class. You  only need to have installed and added to your path Usearch (http://www.drive5.com/usearch/download.html) and BBMap (https://sourceforge.net/projects/bbmap/). To run it you must also provide the following as parameters:

* fastq file (input_fastq) 
* the minimun size of reads to be accepted (amplicon_min_len)
* reference in fasta format (reference)
* Discard reads with > E expected errors per base (max_error_rate=0.05)
* percentage of similarity between the read and the reference to align using BBMap (minid=0.6)
* trim regions with average quality below this (trimq=20)
* minimun average quality (maq=20)
* amount of RAM memory to use when mapping reads (xmx='3g')
* similarity percentage for reads when clustering (clusters_identity=1.0)
* minimun percentage for clusters to accept (filter_perc=0.02)
* remove intermidiate files generated during through pipeline (remove_files=True)

The first three parameters are mandatory while the remaining ones if not provided would run with the default values (as described sabove).

eg.
```python
create_input_for_clusters_class(input_fastq, amplicon_min_len, reference, max_error_rate=0.05, 
                                    minid=0.6, trimq=20, maq=20, xmx='3g', clusters_identity=1.0,
                                    filter_perc=0.02, remove_files=True)
```



## Input file for Pileup Class

The input file for the Pileup Class must be in mpileup format. The mpileup file can be generated from a sam/bam file using SAMtools. The information of stacked bases for each position in the alignment is used to calculated intrapopulation 	heterogeneity. To do it first you need to create a sam file using your reads mapped to a reference. You can use any mapping tool, but I would advise you to use bbmap (https://sourceforge.net/projects/bbmap/), because it is a fast and consistent sequence mapper.
```
$. bbmap/bbmap.sh ref=[reference.fasta] in1=[input.fastq] out=[output.sam] outputunmapped=f minid=0.6 qtrim=t trimq=20 maq=20 ordered=t -Xmx3g
```
It is important to use a reference sequence with trimmed primer regions. Otherwise when you generate a mpileup from your alignment, you would get no information from the bases on the primer region because you trimmed the reads before.

The output from bbmap must be a sam file. Next step is to use SAMtools to generate the mpileup file. First you need to convert the sam file to bam.
```
$ samtools view -S -b [file.sam] > [file.bam]
```
And sort bam file.

For SAMtools 1.3:
```
$ samtools sort -o [sorted.bam] [file.bam]
```
For SAMtools 1.2:
```	
$ samtools sort [file.bam] [file_sorted]
```
Next, you need to index the sorted bam file:
```
$ samtools index -b [sorted.bam]
```
And finally generate the mpileup file:
```
$ samtools mpileup -B -d 10000 -f <reference.fasta> <sorted_indexed.bam> -o [output.mpileup]
```
You can also use the function 'create_input_for_pileup_class' from quasispycies module that recreate the above described steps. You just have to provide a SAM file, a reference fasta and depth for the pileup as inputs and you would get pileup file needed to use on Pileup Class.
```python
create_input_for_pileup_class(input_sam, depth, reference)
```
It's importante to have SAMTools installed on your system and the pathe to the executable added to your path list.
For more information about SAMtools please access (http://www.htslib.org/doc/samtools.html)


## Using the quasispycies module

I recommend you to use a Python notebook system like iPython (https://ipython.org/) to work with your data. IPython is much more flexible than the regular Python IDLE and it helps you store, organize and test your blocks of code in a much more consistent way.

To use the quasispycies module you can save it in your working directory or copy it to the folder where the other packages and modules are stored on your computer. To get all the paths saved on your system, you can type on your Python3 environment:
```python
import sys
sys.path
```
To load the quasispycies module you can type:
```python
import quasispycies as qp
```
And you would have to use 'qp.' preceding each function or class you use.

eg.
```python
pileup_A = qp.Pileup("mpileup_file_A")
qp.fastq_to_fasta("fastq_file", "fasta_file")
```
Otherwise you can use:
```python
from quasispycies import *
```
And all the classes and functions would be loaded from the module and it would not be necessary to use the 'qp.' preceding the calls.


## Brief description of the classes and functions

To get more details about the classes and functions in the module, you can type help() and the respective name inside the brackets to print the docstrings.

eg.
```python
help(Clusters)
help(sam_to_fasta)
```

























